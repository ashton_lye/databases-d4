--Deliverable 2 - Databases - Ashton Lye 9997847

--DROP DATABASE DRIVING_SCHOOL
--CREATE DATABASE DRIVING_SCHOOL

--USE DRIVING_SCHOOL

--EXEC sp_droptype NAME
--EXEC sp_droptype LOGIN
--GO

--EXEC sp_addtype NAME, 'VARCHAR(30)','NOT NULL'
--EXEC sp_addtype LOGIN, 'VARCHAR(20)','NOT NULL'
--GO

CREATE TABLE Admin (
admin_username LOGIN PRIMARY KEY,
password LOGIN,
sname NAME,
fname NAME,
email VARCHAR(30) NOT NULL,
)

CREATE TABLE Instructor (
instructor_username LOGIN PRIMARY KEY,
password LOGIN,
sname NAME,
fname NAME,
email VARCHAR(30) NOT NULL,
phone INT NOT NULL,
)

CREATE TABLE Type (
type_name VARCHAR(20) PRIMARY KEY,
cost INT NOT NULL,
hours INT NOT NULL,
)

CREATE TABLE Car (
licence VARCHAR(6) PRIMARY KEY,
make VARCHAR(20)
)

CREATE TABLE Client (
client_username LOGIN PRIMARY KEY,
password LOGIN,
sname NAME,
fname NAME,
email VARCHAR(30) NOT NULL,
phone INT NOT NULL,
type_name VARCHAR(20) NOT NULL,
FOREIGN KEY (type_name) REFERENCES Type,
)

CREATE TABLE Appointment (
id INT PRIMARY KEY,
notes VARCHAR(140),
confirmed VARCHAR(3),
appt_time VARCHAR(50) NOT NULL,
appt_date DATE NOT NULL,
client_username LOGIN,
instructor_username LOGIN,
FOREIGN KEY (client_username)	REFERENCES Client,
FOREIGN KEY (instructor_username) REFERENCES Instructor,
)

CREATE TABLE Document (
doc_id INT PRIMARY KEY,
amount INT NOT NULL,
paid VARCHAR(3) NOT NULL,
appt_id INT NOT NULL,
client_username LOGIN NOT NULL,
FOREIGN KEY (appt_id) REFERENCES Appointment,
FOREIGN KEY (client_username) REFERENCES Client,
)

CREATE TABLE Leave (
id INT PRIMARY KEY,
leave_time VARCHAR(50) NOT NULL,
leave_date DATE NOT NULL,
instructor_username LOGIN,
FOREIGN KEY (instructor_username) REFERENCES Instructor,
)

CREATE TABLE Assigned(
instructor_username LOGIN NOT NULL,
licence VARCHAR(6) NOT NULL,
id INT NOT NULL,
FOREIGN KEY (instructor_username) REFERENCES Instructor,
FOREIGN KEY (licence) REFERENCES Car,
FOREIGN KEY (id) REFERENCES Appointment,
)

INSERT INTO Instructor VALUES ('instructor','instructor','Instructor','Instructor','instructor@drivingschool.com', 0212348765)
INSERT INTO Instructor VALUES ('jhalpert','jim123', 'Halpert', 'Jim', 'jhalpert@drivingschool.com', 0211234567)
INSERT INTO Instructor VALUES ('dschrute','dwight123','Schrute','Dwight','dschrute@drivingschool.com', 0223217654)
INSERT INTO Instructor VALUES ('abernard','andy123','Bernard','Andy','abernard@drivingschool.com', 0279876543)
INSERT INTO Instructor VALUES ('pvance','phyllis123','Vance','Phyllis','pvance@drivingschool.com', 0214563245)

INSERT INTO Admin VALUES ('admin','admin','Admin','Admin','admin@drivingschool.com')
INSERT INTO Admin VALUES ('phalpert','pam123','Halpert','Pam','phalpert@drivingschool.com')
INSERT INTO Admin VALUES ('mscott','michael123','Scott','Michael','mscott@drivingschool.com')
INSERT INTO Admin VALUES ('ehannon','erin123','Hannon','Erin','ehannon@drivingschool.com')

INSERT INTO Car VALUES ('ABC123','Toyota')
INSERT INTO Car VALUES ('DEF456','Honda')
INSERT INTO Car VALUES ('GHI789','Ford')
INSERT INTO Car VALUES ('JKL987','Holden')
INSERT INTO Car VALUES ('MNO654','Nissan')

INSERT INTO Type VALUES ('experienced',100,2)
INSERT INTO Type VALUES ('new',200,5)

INSERT INTO Client VALUES ('client','client','Client','Client','client@client.com', 0211234567, 'Experienced')
INSERT INTO Client VALUES ('kevin','kevin123','Malone','Kevin','kevinm@gmail.com', 0229876543, 'New')
INSERT INTO Client VALUES ('oscar','oscar123','Martinez','Oscar','oscarmart@gmail.com', 0273453455, 'Experienced')
INSERT INTO Client VALUES ('kelly','kelly123','Kapoor','Kelly','kkapoor@gmail.com', 0225678976, 'New')
INSERT INTO Client VALUES ('toby','toby123','Flenderson','Toby','tflenderson@gmail.com', 0278763456, 'Experienced')

INSERT INTO Appointment VALUES (2, NULL, NULL, '07:00-08:00', '2017-06-30', 'kevin', 'jhalpert')
INSERT INTO Appointment VALUES (3, NULL, NULL, '08:00-09:00', '2017-07-01', 'kevin', 'dschrute')
INSERT INTO Appointment VALUES (4, NULL, NULL, '09:00-10:00', '2017-07-03', 'kevin', 'abernard')
INSERT INTO Appointment VALUES (5, NULL, NULL, '10:00-11:00', '2017-07-04', 'kevin', 'pvance')
INSERT INTO Appointment VALUES (6, NULL, NULL, '11:00-12:00', '2017-07-05', 'oscar', 'jhalpert')
INSERT INTO Appointment VALUES (7, NULL, NULL, '12:00-13:00', '2017-07-06', 'oscar', 'dschrute')
INSERT INTO Appointment VALUES (8, NULL, NULL, '13:00-14:00', '2017-07-07', 'oscar', 'abernard')
INSERT INTO Appointment VALUES (9, NULL, NULL, '14:00-15:00', '2017-07-08', 'oscar', 'pvance')
INSERT INTO Appointment VALUES (10, NULL, NULL, '15:00-16:00', '2017-07-09', 'kelly', 'jhalpert')
INSERT INTO Appointment VALUES (11, NULL, NULL, '16:00-17:00', '2017-07-10', 'kelly', 'dschrute')
INSERT INTO Appointment VALUES (12, NULL, NULL, '17:00-18:00', '2017-07-11', 'kelly', 'abernard')
INSERT INTO Appointment VALUES (13, NULL, NULL, '18:00-19:00', '2017-07-12', 'kelly', 'pvance')
INSERT INTO Appointment VALUES (14, NULL, NULL, '19:00-20:00', '2017-07-13', 'toby', 'jhalpert')
INSERT INTO Appointment VALUES (15, NULL, NULL, '07:00-08:00', '2017-07-14', 'toby', 'dschrute')
INSERT INTO Appointment VALUES (16, NULL, NULL, '08:00-09:00', '2017-07-15', 'toby', 'abernard')
INSERT INTO Appointment VALUES (17, NULL, NULL, '09:00-10:00', '2017-07-16', 'toby', 'pvance')

INSERT INTO Document VALUES (1, 200, 'no', 2, 'kevin')
INSERT INTO Document VALUES (2, 200, 'no', 3, 'kevin')
INSERT INTO Document VALUES (3, 200, 'no', 4, 'kevin')
INSERT INTO Document VALUES (4, 200, 'no', 5, 'kevin')
INSERT INTO Document VALUES (5, 100, 'yes', 6, 'oscar')
INSERT INTO Document VALUES (6, 100, 'yes', 7, 'oscar')
INSERT INTO Document VALUES (7, 100, 'yes', 8, 'oscar')
INSERT INTO Document VALUES (8, 100, 'no', 9, 'oscar')
INSERT INTO Document VALUES (9, 200, 'yes', 10, 'kelly')
INSERT INTO Document VALUES (10, 200, 'yes', 11, 'kelly')
INSERT INTO Document VALUES (11, 200, 'no', 12, 'kelly')
INSERT INTO Document VALUES (12, 200, 'no', 13, 'kelly')
INSERT INTO Document VALUES (13, 100, 'yes', 14, 'toby')
INSERT INTO Document VALUES (14, 100, 'yes', 15, 'toby')
INSERT INTO Document VALUES (15, 100, 'yes', 16, 'toby')
INSERT INTO Document VALUES (16, 100, 'no', 17, 'toby')


SELECT * FROM Admin
SELECT * FROM Instructor
SELECT * FROM Type
SELECT * FROM Document
SELECT * FROM Car
SELECT * FROM Client
SELECT * FROM Appointment
SELECT * FROM Assigned
SELECT * FROM Leave


--DROP TABLE Appointment
--DROP TABLE Client
--DROP TABLE Assigned
--DROP TABLE Admin
--DROP TABLE Instructor
--DROP TABLE Type
--DROP TABLE Document
--DROP TABLE Car
--DROP TABLE Leave
