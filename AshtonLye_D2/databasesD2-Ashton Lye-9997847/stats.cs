﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class stats : Form
    {
        public stats()
        {
            InitializeComponent();

            var totalClients = clientTableAdapter.ScalarQueryCountClients().ToString();
            var totalExpClients = clientTableAdapter.ScalarQueryCountType("Experienced").ToString();
            var totalNewClients = clientTableAdapter.ScalarQueryCountType("New").ToString();

            labelClients.Text = totalClients;
            labelExpClients.Text = totalExpClients;
            labelNewClients.Text = totalNewClients;

            var totalAppts = appointmentTableAdapter.ScalarQueryCountAppts().ToString();

            labelAppts.Text = totalAppts;
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void stats_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'instructorDataSet1.Instructor' table. You can move, or remove it, as needed.
            this.instructorTableAdapter.Fill(this.instructorDataSet1.Instructor);
            // TODO: This line of code loads data into the 'appointmentDataSet.Appointment' table. You can move, or remove it, as needed.
            this.appointmentTableAdapter.Fill(this.appointmentDataSet.Appointment);
            // TODO: This line of code loads data into the 'clientDataSet.Client' table. You can move, or remove it, as needed.
            this.clientTableAdapter.Fill(this.clientDataSet.Client);

        }

        private void comboBoxInstructor_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var selectedInstructor = comboBoxInstructor.SelectedValue.ToString();

                labelInstructorAppts.Text = appointmentTableAdapter.ScalarQueryCountByInstructor(selectedInstructor).ToString();
                labelInstructorAppts.Show();
            }
            catch(Exception ex)
            {
                labelInstructorAppts.Hide();
            }
 
        }
    }
}
