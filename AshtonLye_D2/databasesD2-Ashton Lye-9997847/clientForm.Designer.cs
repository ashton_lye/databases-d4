﻿namespace databasesD2_Ashton_Lye_9997847
{
    partial class clientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.titleLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxClientUsername = new System.Windows.Forms.TextBox();
            this.clientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.clientDataSet = new databasesD2_Ashton_Lye_9997847.clientDataSet();
            this.clientTableAdapter = new databasesD2_Ashton_Lye_9997847.clientDataSetTableAdapters.ClientTableAdapter();
            this.labelClientUsername = new System.Windows.Forms.Label();
            this.textBoxClientPassword = new System.Windows.Forms.TextBox();
            this.labelClientPassword = new System.Windows.Forms.Label();
            this.buttonClientLogin = new System.Windows.Forms.Button();
            this.buttonClientRegister = new System.Windows.Forms.Button();
            this.buttonClientBack = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.clientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(97, 10);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(690, 67);
            this.titleLabel.TabIndex = 1;
            this.titleLabel.Text = "Driving Instruction Academy";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(363, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 33);
            this.label1.TabIndex = 2;
            this.label1.Text = "Client Login";
            // 
            // textBoxClientUsername
            // 
            this.textBoxClientUsername.Location = new System.Drawing.Point(369, 149);
            this.textBoxClientUsername.Name = "textBoxClientUsername";
            this.textBoxClientUsername.Size = new System.Drawing.Size(139, 20);
            this.textBoxClientUsername.TabIndex = 3;
            // 
            // clientBindingSource
            // 
            this.clientBindingSource.DataMember = "Client";
            this.clientBindingSource.DataSource = this.clientDataSet;
            // 
            // clientDataSet
            // 
            this.clientDataSet.DataSetName = "clientDataSet";
            this.clientDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // clientTableAdapter
            // 
            this.clientTableAdapter.ClearBeforeFill = true;
            // 
            // labelClientUsername
            // 
            this.labelClientUsername.AutoSize = true;
            this.labelClientUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelClientUsername.Location = new System.Drawing.Point(276, 149);
            this.labelClientUsername.Name = "labelClientUsername";
            this.labelClientUsername.Size = new System.Drawing.Size(87, 20);
            this.labelClientUsername.TabIndex = 4;
            this.labelClientUsername.Text = "Username:";
            this.labelClientUsername.Click += new System.EventHandler(this.label2_Click);
            // 
            // textBoxClientPassword
            // 
            this.textBoxClientPassword.Location = new System.Drawing.Point(369, 191);
            this.textBoxClientPassword.Name = "textBoxClientPassword";
            this.textBoxClientPassword.Size = new System.Drawing.Size(139, 20);
            this.textBoxClientPassword.TabIndex = 5;
            this.textBoxClientPassword.UseSystemPasswordChar = true;
            // 
            // labelClientPassword
            // 
            this.labelClientPassword.AutoSize = true;
            this.labelClientPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelClientPassword.Location = new System.Drawing.Point(276, 191);
            this.labelClientPassword.Name = "labelClientPassword";
            this.labelClientPassword.Size = new System.Drawing.Size(82, 20);
            this.labelClientPassword.TabIndex = 6;
            this.labelClientPassword.Text = "Password:";
            // 
            // buttonClientLogin
            // 
            this.buttonClientLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClientLogin.Location = new System.Drawing.Point(369, 234);
            this.buttonClientLogin.Name = "buttonClientLogin";
            this.buttonClientLogin.Size = new System.Drawing.Size(139, 39);
            this.buttonClientLogin.TabIndex = 7;
            this.buttonClientLogin.Text = "Login";
            this.buttonClientLogin.UseVisualStyleBackColor = true;
            this.buttonClientLogin.Click += new System.EventHandler(this.buttonClientLogin_Click);
            // 
            // buttonClientRegister
            // 
            this.buttonClientRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClientRegister.Location = new System.Drawing.Point(332, 288);
            this.buttonClientRegister.Name = "buttonClientRegister";
            this.buttonClientRegister.Size = new System.Drawing.Size(217, 44);
            this.buttonClientRegister.TabIndex = 10;
            this.buttonClientRegister.Text = "Register as a New Client";
            this.buttonClientRegister.UseVisualStyleBackColor = true;
            this.buttonClientRegister.Click += new System.EventHandler(this.buttonClientRegister_Click);
            // 
            // buttonClientBack
            // 
            this.buttonClientBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClientBack.Location = new System.Drawing.Point(369, 348);
            this.buttonClientBack.Name = "buttonClientBack";
            this.buttonClientBack.Size = new System.Drawing.Size(139, 39);
            this.buttonClientBack.TabIndex = 11;
            this.buttonClientBack.Text = "Back";
            this.buttonClientBack.UseVisualStyleBackColor = true;
            this.buttonClientBack.Click += new System.EventHandler(this.button1_Click);
            // 
            // clientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Brown;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.buttonClientBack);
            this.Controls.Add(this.buttonClientRegister);
            this.Controls.Add(this.buttonClientLogin);
            this.Controls.Add(this.labelClientPassword);
            this.Controls.Add(this.textBoxClientPassword);
            this.Controls.Add(this.labelClientUsername);
            this.Controls.Add(this.textBoxClientUsername);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.titleLabel);
            this.Name = "clientForm";
            this.Text = "clientForm";
            this.Load += new System.EventHandler(this.clientForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.clientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxClientUsername;
        private clientDataSet clientDataSet;
        private System.Windows.Forms.BindingSource clientBindingSource;
        private clientDataSetTableAdapters.ClientTableAdapter clientTableAdapter;
        private System.Windows.Forms.Label labelClientUsername;
        private System.Windows.Forms.TextBox textBoxClientPassword;
        private System.Windows.Forms.Label labelClientPassword;
        private System.Windows.Forms.Button buttonClientLogin;
        private System.Windows.Forms.Button buttonClientRegister;
        private System.Windows.Forms.Button buttonClientBack;
    }
}