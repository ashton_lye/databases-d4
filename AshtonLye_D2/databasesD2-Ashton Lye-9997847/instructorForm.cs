﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class instructorForm : Form
    {
        public instructorForm()
        {
            InitializeComponent();
        }

        private void instructorForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'instructorDataSet1.Instructor' table. You can move, or remove it, as needed.
            this.instructorTableAdapter.Fill(this.instructorDataSet.Instructor);

        }

        private void textBoxInstructorUsername_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttonInstructorBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonInstructorLogin_Click(object sender, EventArgs e)
        {
            //Code that runs when the login button on the instructor page is clicked
            var userName = textBoxInstructorUsername.Text.Trim();
            var password = textBoxInstructorPassword.Text.Trim();



            if (userName == "" || password == "")
            {
                MessageBox.Show("Please enter a valid username and password");
            }
            else
            {
                try
                {
                    instructorTableAdapter.FillByMatchCredentials(instructorDataSet.Instructor, userName, password);
                }
                catch (Exception)
                {
                    MessageBox.Show("Please enter a valid username and password");
                }
                if (instructorDataSet.Instructor.Rows.Count == 1)
                {
                    MessageBox.Show("Logged in Successfully!");

                    instructorFunctions instructorFunctions1 = new instructorFunctions(userName);
                    instructorFunctions1.Show();
                }
                else
                {
                    MessageBox.Show("No user found with those credentials - Please ensure you're entering them correctly!");
                }
            }
           
        }

        private void textBoxInstructorPassword_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
