﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class bills : Form
    {
        private static string uName;

        public bills(string userName)
        {
            InitializeComponent();
            uName = userName;
            appointmentTableAdapter.FillByClientSessions(appointmentDataSet.Appointment, uName);
        }

        private void bills_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'documentDataSet.Document' table. You can move, or remove it, as needed.
            this.documentTableAdapter.Fill(this.documentDataSet.Document);
            // TODO: This line of code loads data into the 'instructorDataSet1.Instructor' table. You can move, or remove it, as needed.
            this.instructorTableAdapter.Fill(this.instructorDataSet1.Instructor);
            // TODO: This line of code loads data into the 'typeDataSet.Type' table. You can move, or remove it, as needed.
            this.typeTableAdapter.Fill(this.typeDataSet.Type);
            // TODO: This line of code loads data into the 'clientDataSet.Client' table. You can move, or remove it, as needed.
            this.clientTableAdapter.Fill(this.clientDataSet.Client);
            // TODO: This line of code loads data into the 'appointmentDataSet.Appointment' table. You can move, or remove it, as needed.
            this.appointmentTableAdapter.Fill(this.appointmentDataSet.Appointment);

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonViewBill_Click(object sender, EventArgs e)
        {
            var sessionTime = listBoxTime.SelectedValue.ToString();
            var sessionDate = listBoxDate.SelectedValue.ToString();
            var sessionInstructor = listBoxInstructor.SelectedValue.ToString();
            var sessionID = appointmentTableAdapter.ScalarQueryGetApptID(sessionTime, sessionDate, sessionInstructor).ToString();

            labelName.Text = uName;
            labelName.Show();
            labelDate.Text = sessionDate;
            labelDate.Show();
            labelTime.Text = sessionTime;
            labelTime.Show();
            labelInstructor.Text = instructorTableAdapter.ScalarQueryGetFname(sessionInstructor).ToString();
            labelInstructor.Show();
            labelExperience.Text = clientTableAdapter.ScalarQueryGetType(uName);
            labelExperience.Show();
            labelAmount.Text = typeTableAdapter.ScalarQueryGetCost(labelExperience.Text).ToString();
            labelAmount.Show();

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

    }
}
