﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class reviewSession : Form
    {
        private static string uName;

        public reviewSession(string userName)
        {
            InitializeComponent();
            var uName = userName;

            appointmentTableAdapter.FillByUsername(appointmentDataSet.Appointment, uName);
        }

        private void reviewSession_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'appointmentDataSet.Appointment' table. You can move, or remove it, as needed.
            this.appointmentTableAdapter.Fill(this.appointmentDataSet.Appointment);

        }


        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonConfirmSession_Click(object sender, EventArgs e)
        {
            var sessionTime = listBoxTime.SelectedValue.ToString();
            var sessionDate = listBoxDate.SelectedValue.ToString();
            var sessionClient = listBoxClient.SelectedValue.ToString();
            try
            {
                appointmentTableAdapter.UpdateQueryConfirmed("yes", sessionTime, sessionDate, sessionClient);
            }
            catch(Exception ex)
            {
                MessageBox.Show($"{ex}");
            }
        }

        private void buttonNotes_Click(object sender, EventArgs e)
        {
            var sessionTime = listBoxTime.SelectedValue.ToString();
            var sessionDate = listBoxDate.SelectedValue.ToString();
            var sessionClient = listBoxClient.SelectedValue.ToString();
            var notes = textBoxNotes.Text;

            try
            {
                appointmentTableAdapter.UpdateQueryNotes(notes, sessionTime, sessionDate, sessionClient);
            }
            catch(Exception ex)
            {
                MessageBox.Show($"{ex}");
            }

        }
    }
}
