﻿namespace databasesD2_Ashton_Lye_9997847
{
    partial class registerInstructorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label client_usernameLabel;
            System.Windows.Forms.Label passwordLabel;
            System.Windows.Forms.Label snameLabel;
            System.Windows.Forms.Label fnameLabel;
            System.Windows.Forms.Label emailLabel;
            System.Windows.Forms.Label phoneLabel;
            this.tableAdapterManager = new databasesD2_Ashton_Lye_9997847.clientDataSetTableAdapters.TableAdapterManager();
            this.clientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.clientDataSet = new databasesD2_Ashton_Lye_9997847.clientDataSet();
            this.clientTableAdapter = new databasesD2_Ashton_Lye_9997847.clientDataSetTableAdapters.ClientTableAdapter();
            this.instructor_usernameTextBox = new System.Windows.Forms.TextBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.snameTextBox = new System.Windows.Forms.TextBox();
            this.fnameTextBox = new System.Windows.Forms.TextBox();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.phoneTextBox = new System.Windows.Forms.TextBox();
            this.buttonRegisterBack = new System.Windows.Forms.Button();
            this.buttonRegisterInstructor = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.titleLabel = new System.Windows.Forms.Label();
            this.instructorDataSet1 = new databasesD2_Ashton_Lye_9997847.instructorDataSet1();
            this.instructorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.instructorTableAdapter = new databasesD2_Ashton_Lye_9997847.instructorDataSet1TableAdapters.InstructorTableAdapter();
            client_usernameLabel = new System.Windows.Forms.Label();
            passwordLabel = new System.Windows.Forms.Label();
            snameLabel = new System.Windows.Forms.Label();
            fnameLabel = new System.Windows.Forms.Label();
            emailLabel = new System.Windows.Forms.Label();
            phoneLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.clientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instructorDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instructorBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ClientTableAdapter = null;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = databasesD2_Ashton_Lye_9997847.clientDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // clientBindingSource
            // 
            this.clientBindingSource.DataMember = "Client";
            this.clientBindingSource.DataSource = this.clientDataSet;
            // 
            // clientDataSet
            // 
            this.clientDataSet.DataSetName = "clientDataSet";
            this.clientDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // clientTableAdapter
            // 
            this.clientTableAdapter.ClearBeforeFill = true;
            // 
            // client_usernameLabel
            // 
            client_usernameLabel.AutoSize = true;
            client_usernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            client_usernameLabel.Location = new System.Drawing.Point(94, 185);
            client_usernameLabel.Name = "client_usernameLabel";
            client_usernameLabel.Size = new System.Drawing.Size(159, 20);
            client_usernameLabel.TabIndex = 44;
            client_usernameLabel.Text = "Instructor Username:";
            // 
            // instructor_usernameTextBox
            // 
            this.instructor_usernameTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Tag", this.instructorBindingSource, "instructor_username", true));
            this.instructor_usernameTextBox.Location = new System.Drawing.Point(259, 185);
            this.instructor_usernameTextBox.Name = "instructor_usernameTextBox";
            this.instructor_usernameTextBox.Size = new System.Drawing.Size(121, 20);
            this.instructor_usernameTextBox.TabIndex = 45;
            // 
            // passwordLabel
            // 
            passwordLabel.AutoSize = true;
            passwordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            passwordLabel.Location = new System.Drawing.Point(171, 211);
            passwordLabel.Name = "passwordLabel";
            passwordLabel.Size = new System.Drawing.Size(82, 20);
            passwordLabel.TabIndex = 46;
            passwordLabel.Text = "Password:";
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(259, 211);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(121, 20);
            this.passwordTextBox.TabIndex = 47;
            // 
            // snameLabel
            // 
            snameLabel.AutoSize = true;
            snameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            snameLabel.Location = new System.Drawing.Point(175, 237);
            snameLabel.Name = "snameLabel";
            snameLabel.Size = new System.Drawing.Size(78, 20);
            snameLabel.TabIndex = 48;
            snameLabel.Text = "Surname:";
            // 
            // snameTextBox
            // 
            this.snameTextBox.Location = new System.Drawing.Point(259, 237);
            this.snameTextBox.Name = "snameTextBox";
            this.snameTextBox.Size = new System.Drawing.Size(121, 20);
            this.snameTextBox.TabIndex = 49;
            // 
            // fnameLabel
            // 
            fnameLabel.AutoSize = true;
            fnameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            fnameLabel.Location = new System.Drawing.Point(163, 263);
            fnameLabel.Name = "fnameLabel";
            fnameLabel.Size = new System.Drawing.Size(90, 20);
            fnameLabel.TabIndex = 50;
            fnameLabel.Text = "First Name:";
            // 
            // fnameTextBox
            // 
            this.fnameTextBox.Location = new System.Drawing.Point(259, 263);
            this.fnameTextBox.Name = "fnameTextBox";
            this.fnameTextBox.Size = new System.Drawing.Size(121, 20);
            this.fnameTextBox.TabIndex = 51;
            // 
            // emailLabel
            // 
            emailLabel.AutoSize = true;
            emailLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            emailLabel.Location = new System.Drawing.Point(501, 185);
            emailLabel.Name = "emailLabel";
            emailLabel.Size = new System.Drawing.Size(115, 20);
            emailLabel.TabIndex = 52;
            emailLabel.Text = "Email Address:";
            // 
            // emailTextBox
            // 
            this.emailTextBox.Location = new System.Drawing.Point(620, 185);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(121, 20);
            this.emailTextBox.TabIndex = 53;
            // 
            // phoneLabel
            // 
            phoneLabel.AutoSize = true;
            phoneLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            phoneLabel.Location = new System.Drawing.Point(497, 211);
            phoneLabel.Name = "phoneLabel";
            phoneLabel.Size = new System.Drawing.Size(119, 20);
            phoneLabel.TabIndex = 54;
            phoneLabel.Text = "Phone Number:";
            // 
            // phoneTextBox
            // 
            this.phoneTextBox.Location = new System.Drawing.Point(620, 211);
            this.phoneTextBox.Name = "phoneTextBox";
            this.phoneTextBox.Size = new System.Drawing.Size(121, 20);
            this.phoneTextBox.TabIndex = 55;
            // 
            // buttonRegisterBack
            // 
            this.buttonRegisterBack.BackColor = System.Drawing.SystemColors.Control;
            this.buttonRegisterBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRegisterBack.Location = new System.Drawing.Point(382, 393);
            this.buttonRegisterBack.Name = "buttonRegisterBack";
            this.buttonRegisterBack.Size = new System.Drawing.Size(139, 39);
            this.buttonRegisterBack.TabIndex = 43;
            this.buttonRegisterBack.Text = "Back";
            this.buttonRegisterBack.UseVisualStyleBackColor = false;
            this.buttonRegisterBack.Click += new System.EventHandler(this.buttonRegisterBack_Click);
            // 
            // buttonRegisterInstructor
            // 
            this.buttonRegisterInstructor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRegisterInstructor.Location = new System.Drawing.Point(330, 315);
            this.buttonRegisterInstructor.Name = "buttonRegisterInstructor";
            this.buttonRegisterInstructor.Size = new System.Drawing.Size(239, 44);
            this.buttonRegisterInstructor.TabIndex = 42;
            this.buttonRegisterInstructor.Text = "Register Instructor";
            this.buttonRegisterInstructor.UseVisualStyleBackColor = true;
            this.buttonRegisterInstructor.Click += new System.EventHandler(this.buttonRegisterInstructor_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(267, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(370, 33);
            this.label2.TabIndex = 41;
            this.label2.Text = "Please Enter Instructor Details:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(314, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(265, 33);
            this.label1.TabIndex = 40;
            this.label1.Text = "Register an Instructor";
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(97, 28);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(690, 67);
            this.titleLabel.TabIndex = 39;
            this.titleLabel.Text = "Driving Instruction Academy";
            // 
            // instructorDataSet1
            // 
            this.instructorDataSet1.DataSetName = "instructorDataSet1";
            this.instructorDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // instructorBindingSource
            // 
            this.instructorBindingSource.DataMember = "Instructor";
            this.instructorBindingSource.DataSource = this.instructorDataSet1;
            // 
            // instructorTableAdapter
            // 
            this.instructorTableAdapter.ClearBeforeFill = true;
            // 
            // registerInstructorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Brown;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(client_usernameLabel);
            this.Controls.Add(this.instructor_usernameTextBox);
            this.Controls.Add(passwordLabel);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(snameLabel);
            this.Controls.Add(this.snameTextBox);
            this.Controls.Add(fnameLabel);
            this.Controls.Add(this.fnameTextBox);
            this.Controls.Add(emailLabel);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(phoneLabel);
            this.Controls.Add(this.phoneTextBox);
            this.Controls.Add(this.buttonRegisterBack);
            this.Controls.Add(this.buttonRegisterInstructor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.titleLabel);
            this.Name = "registerInstructorForm";
            this.Text = "registerInstructorForm";
            this.Load += new System.EventHandler(this.registerInstructorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.clientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instructorDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instructorBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private clientDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingSource clientBindingSource;
        private clientDataSet clientDataSet;
        private clientDataSetTableAdapters.ClientTableAdapter clientTableAdapter;
        private System.Windows.Forms.TextBox instructor_usernameTextBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.TextBox snameTextBox;
        private System.Windows.Forms.TextBox fnameTextBox;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.TextBox phoneTextBox;
        private System.Windows.Forms.Button buttonRegisterBack;
        private System.Windows.Forms.Button buttonRegisterInstructor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label titleLabel;
        private instructorDataSet1 instructorDataSet1;
        private System.Windows.Forms.BindingSource instructorBindingSource;
        private instructorDataSet1TableAdapters.InstructorTableAdapter instructorTableAdapter;
    }
}