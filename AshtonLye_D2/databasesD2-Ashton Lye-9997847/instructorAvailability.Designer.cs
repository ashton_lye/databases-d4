﻿namespace databasesD2_Ashton_Lye_9997847
{
    partial class instructorAvailability
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.titleLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePickerDate = new System.Windows.Forms.DateTimePicker();
            this.listBoxTime = new System.Windows.Forms.ListBox();
            this.comboBoxInstructor = new System.Windows.Forms.ComboBox();
            this.instructorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.instructorDataSet1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.instructorDataSet1 = new databasesD2_Ashton_Lye_9997847.instructorDataSet1();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bookLeaveButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.listBoxName = new System.Windows.Forms.ListBox();
            this.leaveBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.leaveDataSet = new databasesD2_Ashton_Lye_9997847.leaveDataSet();
            this.listBoxDisplayTime = new System.Windows.Forms.ListBox();
            this.listBoxDate = new System.Windows.Forms.ListBox();
            this.leaveTableAdapter = new databasesD2_Ashton_Lye_9997847.leaveDataSetTableAdapters.LeaveTableAdapter();
            this.instructorTableAdapter = new databasesD2_Ashton_Lye_9997847.instructorDataSet1TableAdapters.InstructorTableAdapter();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.instructorNameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.instructorBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instructorDataSet1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instructorDataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leaveBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.leaveDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(97, 10);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(690, 67);
            this.titleLabel.TabIndex = 28;
            this.titleLabel.Text = "Driving Instruction Academy";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(276, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(317, 33);
            this.label1.TabIndex = 29;
            this.label1.Text = "Edit Instructor Availability";
            // 
            // dateTimePickerDate
            // 
            this.dateTimePickerDate.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerDate.Location = new System.Drawing.Point(145, 159);
            this.dateTimePickerDate.Name = "dateTimePickerDate";
            this.dateTimePickerDate.Size = new System.Drawing.Size(200, 26);
            this.dateTimePickerDate.TabIndex = 30;
            // 
            // listBoxTime
            // 
            this.listBoxTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxTime.FormattingEnabled = true;
            this.listBoxTime.ItemHeight = 20;
            this.listBoxTime.Items.AddRange(new object[] {
            "07:00-08:00",
            "08:00-09:00",
            "09:00-10:00",
            "10:00-11:00",
            "11:00-12:00",
            "12:00-13:00",
            "13:00-14:00",
            "14:00-15:00",
            "15:00-16:00",
            "16:00-17:00",
            "17:00-18:00",
            "18:00-19:00",
            "19:00-20:00"});
            this.listBoxTime.Location = new System.Drawing.Point(145, 205);
            this.listBoxTime.Name = "listBoxTime";
            this.listBoxTime.Size = new System.Drawing.Size(200, 84);
            this.listBoxTime.TabIndex = 31;
            // 
            // comboBoxInstructor
            // 
            this.comboBoxInstructor.DataSource = this.instructorBindingSource;
            this.comboBoxInstructor.DisplayMember = "instructor_username";
            this.comboBoxInstructor.FormattingEnabled = true;
            this.comboBoxInstructor.Location = new System.Drawing.Point(145, 118);
            this.comboBoxInstructor.Name = "comboBoxInstructor";
            this.comboBoxInstructor.Size = new System.Drawing.Size(200, 21);
            this.comboBoxInstructor.TabIndex = 32;
            this.comboBoxInstructor.ValueMember = "instructor_username";
            // 
            // instructorBindingSource
            // 
            this.instructorBindingSource.DataMember = "Instructor";
            this.instructorBindingSource.DataSource = this.instructorDataSet1BindingSource;
            // 
            // instructorDataSet1BindingSource
            // 
            this.instructorDataSet1BindingSource.DataSource = this.instructorDataSet1;
            this.instructorDataSet1BindingSource.Position = 0;
            // 
            // instructorDataSet1
            // 
            this.instructorDataSet1.DataSetName = "instructorDataSet1";
            this.instructorDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(44, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 20);
            this.label2.TabIndex = 33;
            this.label2.Text = "Leave Date:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(36, 232);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 20);
            this.label3.TabIndex = 34;
            this.label3.Text = "Leave Hours:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 119);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(127, 20);
            this.label4.TabIndex = 35;
            this.label4.Text = "Instructor Name:";
            // 
            // bookLeaveButton
            // 
            this.bookLeaveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bookLeaveButton.Location = new System.Drawing.Point(145, 308);
            this.bookLeaveButton.Name = "bookLeaveButton";
            this.bookLeaveButton.Size = new System.Drawing.Size(200, 48);
            this.bookLeaveButton.TabIndex = 36;
            this.bookLeaveButton.Text = "Book Leave";
            this.bookLeaveButton.UseVisualStyleBackColor = true;
            this.bookLeaveButton.Click += new System.EventHandler(this.bookLeaveButton_Click);
            // 
            // backButton
            // 
            this.backButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backButton.Location = new System.Drawing.Point(359, 388);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(148, 48);
            this.backButton.TabIndex = 37;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // listBoxName
            // 
            this.listBoxName.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.leaveBindingSource, "instructor_username", true));
            this.listBoxName.DataSource = this.leaveBindingSource;
            this.listBoxName.DisplayMember = "instructor_username";
            this.listBoxName.FormattingEnabled = true;
            this.listBoxName.Location = new System.Drawing.Point(424, 159);
            this.listBoxName.Name = "listBoxName";
            this.listBoxName.Size = new System.Drawing.Size(120, 199);
            this.listBoxName.TabIndex = 38;
            this.listBoxName.ValueMember = "instructor_username";
            // 
            // leaveBindingSource
            // 
            this.leaveBindingSource.DataMember = "Leave";
            this.leaveBindingSource.DataSource = this.leaveDataSet;
            // 
            // leaveDataSet
            // 
            this.leaveDataSet.DataSetName = "leaveDataSet";
            this.leaveDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // listBoxDisplayTime
            // 
            this.listBoxDisplayTime.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.leaveBindingSource, "leave_time", true));
            this.listBoxDisplayTime.DataSource = this.leaveBindingSource;
            this.listBoxDisplayTime.DisplayMember = "leave_time";
            this.listBoxDisplayTime.FormattingEnabled = true;
            this.listBoxDisplayTime.Location = new System.Drawing.Point(550, 159);
            this.listBoxDisplayTime.Name = "listBoxDisplayTime";
            this.listBoxDisplayTime.Size = new System.Drawing.Size(120, 199);
            this.listBoxDisplayTime.TabIndex = 39;
            this.listBoxDisplayTime.ValueMember = "leave_time";
            // 
            // listBoxDate
            // 
            this.listBoxDate.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.leaveBindingSource, "leave_date", true));
            this.listBoxDate.DataSource = this.leaveBindingSource;
            this.listBoxDate.DisplayMember = "leave_date";
            this.listBoxDate.FormattingEnabled = true;
            this.listBoxDate.Location = new System.Drawing.Point(676, 159);
            this.listBoxDate.Name = "listBoxDate";
            this.listBoxDate.Size = new System.Drawing.Size(120, 199);
            this.listBoxDate.TabIndex = 40;
            this.listBoxDate.ValueMember = "leave_date";
            // 
            // leaveTableAdapter
            // 
            this.leaveTableAdapter.ClearBeforeFill = true;
            // 
            // instructorTableAdapter
            // 
            this.instructorTableAdapter.ClearBeforeFill = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(420, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 20);
            this.label5.TabIndex = 41;
            this.label5.Text = "Instructor";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(546, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 20);
            this.label6.TabIndex = 42;
            this.label6.Text = "Time";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(672, 136);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(44, 20);
            this.label7.TabIndex = 43;
            this.label7.Text = "Date";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(531, 116);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(154, 20);
            this.label8.TabIndex = 44;
            this.label8.Text = "Instructors on Leave";
            // 
            // instructorNameLabel
            // 
            this.instructorNameLabel.AutoSize = true;
            this.instructorNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.instructorNameLabel.Location = new System.Drawing.Point(146, 119);
            this.instructorNameLabel.Name = "instructorNameLabel";
            this.instructorNameLabel.Size = new System.Drawing.Size(49, 20);
            this.instructorNameLabel.TabIndex = 45;
            this.instructorNameLabel.Text = "name";
            this.instructorNameLabel.Visible = false;
            // 
            // instructorAvailability
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Brown;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(this.instructorNameLabel);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.listBoxDate);
            this.Controls.Add(this.listBoxDisplayTime);
            this.Controls.Add(this.listBoxName);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.bookLeaveButton);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxInstructor);
            this.Controls.Add(this.listBoxTime);
            this.Controls.Add(this.dateTimePickerDate);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.titleLabel);
            this.Name = "instructorAvailability";
            this.Text = "instructorAvailability";
            this.Load += new System.EventHandler(this.instructorAvailability_Load);
            ((System.ComponentModel.ISupportInitialize)(this.instructorBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instructorDataSet1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instructorDataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leaveBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.leaveDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePickerDate;
        private System.Windows.Forms.ListBox listBoxTime;
        private System.Windows.Forms.ComboBox comboBoxInstructor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button bookLeaveButton;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.ListBox listBoxName;
        private System.Windows.Forms.ListBox listBoxDisplayTime;
        private System.Windows.Forms.ListBox listBoxDate;
        private leaveDataSet leaveDataSet;
        private System.Windows.Forms.BindingSource leaveBindingSource;
        private leaveDataSetTableAdapters.LeaveTableAdapter leaveTableAdapter;
        private System.Windows.Forms.BindingSource instructorDataSet1BindingSource;
        private instructorDataSet1 instructorDataSet1;
        private System.Windows.Forms.BindingSource instructorBindingSource;
        private instructorDataSet1TableAdapters.InstructorTableAdapter instructorTableAdapter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label instructorNameLabel;
    }
}