﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class checkBills : Form
    {
        public checkBills()
        {
            InitializeComponent();
            try
            {
                this.documentTableAdapter.FillBy(this.documentDataSet.Document);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void checkBills_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'documentDataSet.Document' table. You can move, or remove it, as needed.
            this.documentTableAdapter.Fill(this.documentDataSet.Document);

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonMarkPaid_Click(object sender, EventArgs e)
        {
            var billID = int.Parse(listBoxID.SelectedValue.ToString());
            //hahahaha

            documentTableAdapter.UpdateQueryPaid("yes", billID);
            checkBills_Load(sender, e); 
        }
    }
}
