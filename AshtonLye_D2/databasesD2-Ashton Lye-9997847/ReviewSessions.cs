﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class reviewSessions : Form
    {
        public reviewSessions()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void ReviewSessions_Load(object sender, EventArgs e)
        {

        }

        private void fillByUsernameToolStripButton_Click(object sender, EventArgs e)
        {

        }

        private void usernameToolStripTextBox_Click(object sender, EventArgs e)
        {

        }

        private void fillByUsernameToolStripButton1_Click(object sender, EventArgs e)
        {

        }

        private void fillByUsernameToolStripButton2_Click(object sender, EventArgs e)
        {

        }

        private void buttonBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void reviewSessions_Load_1(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'appointmentDataSet.Appointment' table. You can move, or remove it, as needed.
            this.appointmentTableAdapter.Fill(this.appointmentDataSet.Appointment);

        }

        private void fillByUsernameToolStripButton_Click_1(object sender, EventArgs e)
        {
            try
            {
                this.appointmentTableAdapter.FillByUsername(this.appointmentDataSet.Appointment, usernameToolStripTextBox.Text);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void fillByUsernameToolStripButton_Click_2(object sender, EventArgs e)
        {
            try
            {
                this.appointmentTableAdapter.FillByUsername(this.appointmentDataSet.Appointment, usernameToolStripTextBox.Text);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void fillByUsernameToolStripButton1_Click_1(object sender, EventArgs e)
        {
            try
            {
                this.appointmentTableAdapter.FillByUsername(this.appointmentDataSet.Appointment, usernameToolStripTextBox1.Text);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }

        }

        private void buttonBack_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
