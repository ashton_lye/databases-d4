﻿namespace databasesD2_Ashton_Lye_9997847
{
    partial class registerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label client_usernameLabel;
            System.Windows.Forms.Label passwordLabel;
            System.Windows.Forms.Label snameLabel;
            System.Windows.Forms.Label fnameLabel;
            System.Windows.Forms.Label emailLabel;
            System.Windows.Forms.Label phoneLabel;
            System.Windows.Forms.Label type_nameLabel;
            this.titleLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonRegister = new System.Windows.Forms.Button();
            this.buttonRegisterBack = new System.Windows.Forms.Button();
            this.clientDataSet = new databasesD2_Ashton_Lye_9997847.clientDataSet();
            this.clientTableAdapter = new databasesD2_Ashton_Lye_9997847.clientDataSetTableAdapters.ClientTableAdapter();
            this.client_usernameTextBox = new System.Windows.Forms.TextBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.clientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.snameTextBox = new System.Windows.Forms.TextBox();
            this.fnameTextBox = new System.Windows.Forms.TextBox();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.phoneTextBox = new System.Windows.Forms.TextBox();
            this.type_nameComboBox = new System.Windows.Forms.ComboBox();
            this.tableAdapterManager = new databasesD2_Ashton_Lye_9997847.clientDataSetTableAdapters.TableAdapterManager();
            client_usernameLabel = new System.Windows.Forms.Label();
            passwordLabel = new System.Windows.Forms.Label();
            snameLabel = new System.Windows.Forms.Label();
            fnameLabel = new System.Windows.Forms.Label();
            emailLabel = new System.Windows.Forms.Label();
            phoneLabel = new System.Windows.Forms.Label();
            type_nameLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.clientDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // client_usernameLabel
            // 
            client_usernameLabel.AutoSize = true;
            client_usernameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            client_usernameLabel.Location = new System.Drawing.Point(122, 167);
            client_usernameLabel.Name = "client_usernameLabel";
            client_usernameLabel.Size = new System.Drawing.Size(131, 20);
            client_usernameLabel.TabIndex = 25;
            client_usernameLabel.Text = "Client Username:";
            client_usernameLabel.Click += new System.EventHandler(this.client_usernameLabel_Click);
            // 
            // passwordLabel
            // 
            passwordLabel.AutoSize = true;
            passwordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            passwordLabel.Location = new System.Drawing.Point(171, 193);
            passwordLabel.Name = "passwordLabel";
            passwordLabel.Size = new System.Drawing.Size(82, 20);
            passwordLabel.TabIndex = 27;
            passwordLabel.Text = "Password:";
            passwordLabel.Click += new System.EventHandler(this.passwordLabel_Click);
            // 
            // snameLabel
            // 
            snameLabel.AutoSize = true;
            snameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            snameLabel.Location = new System.Drawing.Point(175, 219);
            snameLabel.Name = "snameLabel";
            snameLabel.Size = new System.Drawing.Size(78, 20);
            snameLabel.TabIndex = 29;
            snameLabel.Text = "Surname:";
            snameLabel.Click += new System.EventHandler(this.snameLabel_Click);
            // 
            // fnameLabel
            // 
            fnameLabel.AutoSize = true;
            fnameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            fnameLabel.Location = new System.Drawing.Point(163, 245);
            fnameLabel.Name = "fnameLabel";
            fnameLabel.Size = new System.Drawing.Size(90, 20);
            fnameLabel.TabIndex = 31;
            fnameLabel.Text = "First Name:";
            fnameLabel.Click += new System.EventHandler(this.fnameLabel_Click);
            // 
            // emailLabel
            // 
            emailLabel.AutoSize = true;
            emailLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            emailLabel.Location = new System.Drawing.Point(501, 167);
            emailLabel.Name = "emailLabel";
            emailLabel.Size = new System.Drawing.Size(115, 20);
            emailLabel.TabIndex = 33;
            emailLabel.Text = "Email Address:";
            emailLabel.Click += new System.EventHandler(this.emailLabel_Click);
            // 
            // phoneLabel
            // 
            phoneLabel.AutoSize = true;
            phoneLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            phoneLabel.Location = new System.Drawing.Point(497, 193);
            phoneLabel.Name = "phoneLabel";
            phoneLabel.Size = new System.Drawing.Size(119, 20);
            phoneLabel.TabIndex = 35;
            phoneLabel.Text = "Phone Number:";
            phoneLabel.Click += new System.EventHandler(this.phoneLabel_Click);
            // 
            // type_nameLabel
            // 
            type_nameLabel.AutoSize = true;
            type_nameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            type_nameLabel.Location = new System.Drawing.Point(512, 220);
            type_nameLabel.Name = "type_nameLabel";
            type_nameLabel.Size = new System.Drawing.Size(102, 20);
            type_nameLabel.TabIndex = 37;
            type_nameLabel.Text = "Driving Level:";
            type_nameLabel.Click += new System.EventHandler(this.type_nameLabel_Click);
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLabel.Location = new System.Drawing.Point(97, 10);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(690, 67);
            this.titleLabel.TabIndex = 1;
            this.titleLabel.Text = "Driving Instruction Academy";
            this.titleLabel.Click += new System.EventHandler(this.titleLabel_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(314, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(255, 33);
            this.label1.TabIndex = 9;
            this.label1.Text = "Register a New Client";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Comic Sans MS", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(299, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(305, 33);
            this.label2.TabIndex = 22;
            this.label2.Text = "Please Enter Your Details:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // buttonRegister
            // 
            this.buttonRegister.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRegister.Location = new System.Drawing.Point(330, 297);
            this.buttonRegister.Name = "buttonRegister";
            this.buttonRegister.Size = new System.Drawing.Size(239, 44);
            this.buttonRegister.TabIndex = 23;
            this.buttonRegister.Text = "Register as a New Client";
            this.buttonRegister.UseVisualStyleBackColor = true;
            this.buttonRegister.Click += new System.EventHandler(this.buttonRegister_Click);
            // 
            // buttonRegisterBack
            // 
            this.buttonRegisterBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRegisterBack.Location = new System.Drawing.Point(382, 375);
            this.buttonRegisterBack.Name = "buttonRegisterBack";
            this.buttonRegisterBack.Size = new System.Drawing.Size(139, 39);
            this.buttonRegisterBack.TabIndex = 24;
            this.buttonRegisterBack.Text = "Back";
            this.buttonRegisterBack.UseVisualStyleBackColor = true;
            this.buttonRegisterBack.Click += new System.EventHandler(this.buttonRegisterBack_Click);
            // 
            // clientDataSet
            // 
            this.clientDataSet.DataSetName = "clientDataSet";
            this.clientDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // clientTableAdapter
            // 
            this.clientTableAdapter.ClearBeforeFill = true;
            // 
            // client_usernameTextBox
            // 
            this.client_usernameTextBox.Location = new System.Drawing.Point(259, 167);
            this.client_usernameTextBox.Name = "client_usernameTextBox";
            this.client_usernameTextBox.Size = new System.Drawing.Size(121, 20);
            this.client_usernameTextBox.TabIndex = 26;
            this.client_usernameTextBox.TextChanged += new System.EventHandler(this.client_usernameTextBox_TextChanged);
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.Location = new System.Drawing.Point(259, 193);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(121, 20);
            this.passwordTextBox.TabIndex = 28;
            this.passwordTextBox.TextChanged += new System.EventHandler(this.passwordTextBox_TextChanged);
            // 
            // clientBindingSource
            // 
            this.clientBindingSource.DataMember = "Client";
            this.clientBindingSource.DataSource = this.clientDataSet;
            this.clientBindingSource.CurrentChanged += new System.EventHandler(this.clientBindingSource_CurrentChanged);
            // 
            // snameTextBox
            // 
            this.snameTextBox.Location = new System.Drawing.Point(259, 219);
            this.snameTextBox.Name = "snameTextBox";
            this.snameTextBox.Size = new System.Drawing.Size(121, 20);
            this.snameTextBox.TabIndex = 30;
            this.snameTextBox.TextChanged += new System.EventHandler(this.snameTextBox_TextChanged);
            // 
            // fnameTextBox
            // 
            this.fnameTextBox.Location = new System.Drawing.Point(259, 245);
            this.fnameTextBox.Name = "fnameTextBox";
            this.fnameTextBox.Size = new System.Drawing.Size(121, 20);
            this.fnameTextBox.TabIndex = 32;
            this.fnameTextBox.TextChanged += new System.EventHandler(this.fnameTextBox_TextChanged);
            // 
            // emailTextBox
            // 
            this.emailTextBox.Location = new System.Drawing.Point(620, 167);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(121, 20);
            this.emailTextBox.TabIndex = 34;
            this.emailTextBox.TextChanged += new System.EventHandler(this.emailTextBox_TextChanged);
            // 
            // phoneTextBox
            // 
            this.phoneTextBox.Location = new System.Drawing.Point(620, 193);
            this.phoneTextBox.Name = "phoneTextBox";
            this.phoneTextBox.Size = new System.Drawing.Size(121, 20);
            this.phoneTextBox.TabIndex = 36;
            this.phoneTextBox.TextChanged += new System.EventHandler(this.phoneTextBox_TextChanged);
            // 
            // type_nameComboBox
            // 
            this.type_nameComboBox.FormattingEnabled = true;
            this.type_nameComboBox.Items.AddRange(new object[] {
            "Experienced",
            "New"});
            this.type_nameComboBox.Location = new System.Drawing.Point(620, 219);
            this.type_nameComboBox.Name = "type_nameComboBox";
            this.type_nameComboBox.Size = new System.Drawing.Size(121, 21);
            this.type_nameComboBox.TabIndex = 38;
            this.type_nameComboBox.SelectedIndexChanged += new System.EventHandler(this.type_nameComboBox_SelectedIndexChanged);
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ClientTableAdapter = null;
            this.tableAdapterManager.Connection = null;
            this.tableAdapterManager.UpdateOrder = databasesD2_Ashton_Lye_9997847.clientDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // registerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Brown;
            this.ClientSize = new System.Drawing.Size(884, 461);
            this.Controls.Add(client_usernameLabel);
            this.Controls.Add(this.client_usernameTextBox);
            this.Controls.Add(passwordLabel);
            this.Controls.Add(this.passwordTextBox);
            this.Controls.Add(snameLabel);
            this.Controls.Add(this.snameTextBox);
            this.Controls.Add(fnameLabel);
            this.Controls.Add(this.fnameTextBox);
            this.Controls.Add(emailLabel);
            this.Controls.Add(this.emailTextBox);
            this.Controls.Add(phoneLabel);
            this.Controls.Add(this.phoneTextBox);
            this.Controls.Add(type_nameLabel);
            this.Controls.Add(this.type_nameComboBox);
            this.Controls.Add(this.buttonRegisterBack);
            this.Controls.Add(this.buttonRegister);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.titleLabel);
            this.Name = "registerForm";
            this.Text = "registerForm";
            this.Load += new System.EventHandler(this.registerForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.clientDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonRegister;
        private System.Windows.Forms.Button buttonRegisterBack;
        private clientDataSet clientDataSet;
        private System.Windows.Forms.BindingSource clientBindingSource;
        private clientDataSetTableAdapters.ClientTableAdapter clientTableAdapter;
        private clientDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.TextBox client_usernameTextBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.TextBox snameTextBox;
        private System.Windows.Forms.TextBox fnameTextBox;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.TextBox phoneTextBox;
        private System.Windows.Forms.ComboBox type_nameComboBox;
    }
}