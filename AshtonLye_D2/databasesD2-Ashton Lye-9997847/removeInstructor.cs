﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace databasesD2_Ashton_Lye_9997847
{
    public partial class removeInstructor : Form
    {
        public removeInstructor()
        {
            InitializeComponent();
        }

        private void removeInstructor_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'instructorDataSet1.Instructor' table. You can move, or remove it, as needed.
            this.instructorTableAdapter.Fill(this.instructorDataSet1.Instructor);

        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {
            var instructor = comboBoxInstructor.SelectedValue.ToString();

            try
            {
                instructorTableAdapter.DeleteQueryRemoveInstructor(instructor);
                MessageBox.Show("Instructor Deleted Successfully!");
            }
            catch(Exception ex)
            {
                MessageBox.Show($"{ex}");
            }
            
        }
    }
}
